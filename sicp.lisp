(defun square (x)
  (* x x))

(defun sum-of-squares (x y)
  (+ (square x) (square y)))

(defun my-abs (x)
  (if (< x 0)
      (- x)
      x))

;; Problem 1.2
(defthm probolem-1-2
  (equal (/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5)))))
            (* (- 6 2) (- 2 7)))
         -37/50)
  :rule-classes nil)

;; Problem 1.3
(defun max-sum-of-squares (x y z)
  (if (< x y)
      (if (< x z)
          (sum-of-squares y z)
          (sum-of-squares y x))
      (if (< y z)
          (sum-of-squares x z)
          (sum-of-squares x y))))

(defthm theorem-of-max-sum-of-squares
  (implies (and (rationalp x)
                (rationalp y)
                (rationalp z)
                (<= x y)
                (<= x z))
           (let ((answer (sum-of-squares y z)))
             (and (equal (max-sum-of-squares x y z) answer)
                  (equal (max-sum-of-squares y x z) answer)
                  (equal (max-sum-of-squares y z x) answer))))
  :rule-classes nil)

(defun factorial (n)
  (if (zp n)
      1
      (* n (factorial (1- n)))))

(defun fact-iter (product n)
  (if (zp n)
      product
      (fact-iter (* n product) (1- n))))

(include-book "arithmetic/top-with-meta" :dir :system)

(defthm factorial-equal-fact-iter-*
  (implies (and (natp m)
                (natp n))
           (equal (fact-iter m n)
                  (* m (factorial n)))))

(defthm factorial-equal-fact-iter
  (implies (and (natp m)
                (natp n))
           (equal (fact-iter 1 n)
                  (factorial n))))

(defun plus (a b)
  (if (zp a)
      b
      (1+ (plus (1- a) b))))

(defun plus-iter (a b)
  (if (zp a)
      b
      (plus-iter (1- a) (1+ b))))

(defthm plus-equal-plus-iter
  (implies (and (natp a)
                (natp b))
           (equal (plus-iter a b)
                  (plus a b))))

(defun ackermann-measure (x y)
  (if (and (posp x)
           (natp y))
      (make-ord 1 x y)
      0))

(defun ackermann (x y)
  (declare (xargs
            :measure (ackermann-measure x y)))
  (cond ((zp y) 0)
        ((zp x) (* 2 y))
        ((= y 1) 2)
        (t (ackermann (- x 1)
                      (ackermann x (- y 1))))))

;; Problem 1.10
(defthm simple-definition-of-ackermann-f
  (implies (posp n)
           (equal (ackermann 0 n)
                  (* 2 n))))

(defthm simple-definition-of-ackermann-g
  (implies (posp n)
           (equal (ackermann 1 n)
                  (expt 2 n))))

(defun hyper4 (x y)
  (if (zp y)
      1
      (expt x (hyper4 x (1- y)))))

(defthm simple-definition-of-ackermann-h
  (implies (posp n)
           (equal (ackermann 2 n)
                  (hyper4 2 n)))
  :hints (("Goal" :expand (ackermann 2 n))))

(defun fib* (a b n)
  (cond ((zp n) b)
        ((= n 1) a)
        (t (+ (fib* a b (- n 1))
              (fib* a b (- n 2))))))

(defun fib-iter (a b count)
  (if (zp count)
      b
      (fib-iter (+ a b) a (- count 1))))

(defthm fib-iter-equal-fib*-lemma-1
  (implies (and (posp a)
                (natp b))
           (equal (fib-iter a b 1) a))
  :hints (("Goal" :expand (fib-iter a b 1))))

(defthm fib-iter-equal-fib*
  (implies (and (natp n)
                (posp a)
                (natp b))
           (equal (fib-iter a b n)
                  (fib* a b n))))

(defun fib (n)
  (fib* 1 0 n))

(defthm fib-iter-equal-fib
  (implies (natp n)
           (equal (fib-iter 1 0 n)
                  (fib n))))

(defconst *kinds-of-coins* '(1 5 10 25 50))

(defun count-change-mesure (amount kinds-of-coins)
  (if (and (natp amount)
           (consp kinds-of-coins))
      (make-ord 1 (acl2-count kinds-of-coins) amount)
      0))

(defun count-change (amount kinds-of-coins)
  (declare (xargs :measure (count-change-mesure amount kinds-of-coins)
                  :hints (("Goal" :expand ((ACL2-COUNT KINDS-OF-COINS))))))
  (cond ((zip amount) 1)
        ((or (< amount 0) (endp kinds-of-coins)) 0)
        ((<= (car kinds-of-coins) 0) 0)
        (t (+ (count-change amount
                            (cdr kinds-of-coins))
              (count-change (- amount (car kinds-of-coins))
                            kinds-of-coins)))))

(defun problem-1.11-recur-* (n a b c)
  (cond ((zp n) c)
        ((= n 1) b)
        ((= n 2) a)
        (t (+ (problem-1.11-recur-* (- n 1) a b c)
              (* 2 (problem-1.11-recur-* (- n 2) a b c))
              (* 3 (problem-1.11-recur-* (- n 3) a b c))))))

(defun problem-1.11-recur (n)
  (problem-1.11-recur-* n 2 1 0))

(defun problem-1.11-iter (n fn-1 fn-2 fn-3)
  (cond ((zp n) fn-3)
        ((= n 1) fn-2)
        ((= n 2) fn-1)
        (t (problem-1.11-iter (- n 1)
                              (+ fn-1 (* 2 fn-2) (* 3 fn-3))
                              fn-1
                              fn-2))))

;; (defthm problem-1.11-iter-equal-problem-1.11-recur-*
;;   (implies (and (natp n)
;;                 (natp a)
;;                 (natp b)
;;                 (natp c))
;;            (equal (problem-1.11-recur-* n a b c)
;;                   (problem-1.11-iter n a b c))))

;; Problem-1.13
(defun pascal-triangle-measure (x y)
  (cond ((and (zp x) (zp y)) 0)
        ((zp x) y)
        ((zp y) x)
        (t (+ x y))))

(defun pascal-triangle (x y)
  (declare (xargs :measure (pascal-triangle-measure x y)))
  (cond ((and (zp x) (zp y)) 1)
        ((zp x) (+ 1 (pascal-triangle 0 (- y 1))))
        ((zp y) (+ (pascal-triangle (- x 1) 0) 1))
        (t (+ (pascal-triangle (- x 1) y)
              (pascal-triangle x (- y 1))))))

(defthm theorem-of-pascal-triangle
  (implies (and (posp x)
                (posp y))
           (equal (pascal-triangle x y)
                  (+ (pascal-triangle (- x 1) y)
                     (pascal-triangle x (- y 1))))))

(defthm commutativity-of-pascal-triangle
  (implies (and (natp x)
                (natp y))
           (equal (pascal-triangle x y)
                  (pascal-triangle y x))))

(defun sicp-expt (b n)
  (if (zp n)
      1
      (* b (sicp-expt b (- n 1)))))

(defun sicp-expt-iter (b counter product)
  (if (zp counter)
      product
      (sicp-expt-iter b
                      (- counter 1)
                      (* b product))))

(defun fast-expt (b n)
  (cond ((zp n) 1)
        ((evenp n) (square (fast-expt b (/ n 2))))
        (t (* b (fast-expt b (- n 1))))))
